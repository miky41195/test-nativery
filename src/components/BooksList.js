import React, { useContext } from 'react';
import _ from 'lodash';
import Book from './Book';
import BooksContext from '../context/BooksContext';

const BooksList = () => {
  const { books, setBooks } = useContext(BooksContext);

  const handleRemoveBook = (id) => {
    setBooks(books.filter((book) => book.id !== id));
  };
  var bookExample = {
    "id": "f054dc78-36da-4d9f-8d93-0ddc4b868c36",
    "bookname": "Test Nativery",
    "author": "Michele Brescia",
    "price": "100",
    "quantity": "1",
    "date": "2021-10-02T16:49:54.328Z"
};
document.onreadystatechange = function(e)
{
  if(books.length <= 0)
  books.push(bookExample);
  console.log(books)
};
  return (
    <React.Fragment>
      <div className="book-list">
        {!_.isEmpty(books) ? (
          books.map((book) => (
            <Book key={book.id} {...book} handleRemoveBook={handleRemoveBook} />
          ))
        ) : (
          <p className="message">No books available. Please add some books.</p>
        )}
      </div>
    </React.Fragment>
  );
};
export default BooksList;
