import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useTour } from '@reactour/tour'


const Header = () => {
    const { setIsOpen } = useTour()
  return (
    <header>
      <h1>CRUD App</h1>
      <hr />
      <div className="links">
        <NavLink to="/" className="link bookList" activeClassName="active" exact>
          Books List
        </NavLink>
        <NavLink to="/add" className="link add" activeClassName="active">
          Add Book
        </NavLink>
        <button className="link" id="btnTour" hidden="{true}" onClick={() => setIsOpen(true)} title={'Tour'}>Take a Tour</button>
      </div>
    </header>
  );
};

window.onload = function(){
  var button = document.getElementById('btnTour').click();
}

export default Header;