import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './router/AppRouter';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.scss';
import { useState } from "react";
import Tour from 'reactour';
import { TourProvider } from '@reactour/tour'
import Header from './components/Header.js';

const steps = [
    {
      selector: ".bookList",
      content: "Permette di visualizzare la lista di Libri inseriti.",
    },
    {
      selector: ".add",
      content: "Permette di visualizzare un form dove aggiungere un nuovo libro.",
    },
    {
      selector: ".card-body",
      content: "Questo è il corpo di un libro dopo essere stato creato.",
    },
    {
      selector: ".btn-primary",
      content: "Permette di modificare un libro già creato in precedenza.",
    },
    {
      selector: ".btn-danger",
      content: "Permette di rimuovere un libro dalla lista.",
    }
  ];
  ReactDOM.render(
  <TourProvider steps={steps} >
    <AppRouter />
  </TourProvider>,
  document.getElementById('root')
)
//ReactDOM.render(<AppRouter />, document.getElementById('root'));